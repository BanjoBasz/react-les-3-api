<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Winner extends Model
{
    protected $table = "winners";

    public function getVideo(){
      return $this->hasOne('App\Video','src','video');
    }
}
