<?php

use Illuminate\Database\Seeder;

class VideosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('videos')->insert([
        "src" => "/videos/2006.mp4",
        "description" => "Finlands performance on EV06",
      ]);

      DB::table('videos')->insert([
        "src" => "/videos/2019.mp4",
        "description" => "The Netherlands performance in 2019",
      ]);

      DB::table('videos')->insert([
        "src" => "/videos/2009.mp4",
        "description" => "Fairytale",
      ]);

      DB::table('videos')->insert([
        "src" => "/videos/2012.mp4",
        "description" => "Euphoria Final song",
      ]);
    }
}
