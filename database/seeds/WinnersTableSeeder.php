<?php

use Illuminate\Database\Seeder;

class WinnersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      DB::table('winners')->insert([
        "country" => "Finland",
        "artist" => "Lordi",
        "song" => "Hard Rock Hallelujah",
        "video" => "/videos/2006.mp4",
        "year" => 2006,
      ]);

      DB::table('winners')->insert([
        "country" => "The Netherlands",
        "artist" => "Duncan Laurence",
        "song" => "Arcade",
        "video" => "/videos/2019.mp4",
        "year" => 2019,
      ]);

      DB::table('winners')->insert([
        "country" => "Norway",
        "artist" => "Alexander Rybak",
        "song" => "Fairytale",
        "video" => "/videos/2009.mp4",
        "year" => 2009,
      ]);

      DB::table('winners')->insert([
        "country" => "Sweden",
        "artist" => "Loreen",
        "song" => "Euphoria",
        "video" => "/videos/2012.mp4",
        "year" => 2012,
      ]);
    }
}
